const header__burger = document.querySelector('.header-burger__burger');
const header__menu = document.querySelector('.header-burger__menu')
const header__list = document.querySelector('.header-burger__list')
const header__btn = document.querySelector('#header-burger__btn')
const header__logo = document.querySelector('.header-burger__logo')
const squadArrow = document.querySelector('.squad-arrow')
const squadList = document.querySelectorAll('.squad-col')
const form = document.querySelector('.form')
const select = document.querySelector('._sel')
const options = document.querySelectorAll('.form-select__option')
const btnDown = document.querySelector('.intro-down')
const header = document.querySelector('.header')
const tabsInner = document.querySelector('.tabs-inner')

const models3d = document.querySelectorAll('.intro-model3d')
const spinners = document.querySelectorAll('.intro-preloader')
const spinner = (document.documentElement.clientWidth < 575) ? spinners[1] : spinners[0]
const model3D = (document.documentElement.clientWidth < 575) ? models3d[1] : models3d[0]

function toggleDisplayStyle(elem, displayValue) {
    return elem.style.display = displayValue
}

const displayValue = {
    none: 'none',
    flex: 'flex',
    block: 'block'
}

model3D.addEventListener('load', () => {
    toggleDisplayStyle(spinner, displayValue.none)
    toggleDisplayStyle(model3D, displayValue.flex)
})


const telegramInput = document.querySelector('#telegram')

telegramInput.addEventListener('input', (event) => {
    let value = event.target.value
    if (value[0] !== '@') {
        telegramInput.value = `${"@"}${value}`
    }
})

telegramInput.addEventListener('keyup', (event) => {
    let value = event.target.value
    if (event.keyCode === 8 && value[0] === '@' && !value[1]) {
        telegramInput.value = ''
    }
})

var Visible = function (target) {

    var targetPosition = {
            top: window.pageYOffset + target.getBoundingClientRect().top,
            left: window.pageXOffset + target.getBoundingClientRect().left,
            right: window.pageXOffset + target.getBoundingClientRect().right,
            bottom: window.pageYOffset + target.getBoundingClientRect().bottom
        },

        windowPosition = {
            top: window.pageYOffset,
            left: window.pageXOffset,
            right: window.pageXOffset + document.documentElement.clientWidth,
            bottom: window.pageYOffset + document.documentElement.clientHeight
        };

    if (targetPosition.bottom > windowPosition.top &&
        targetPosition.top < windowPosition.bottom) {

        header.style.position = 'absolute'
    } else {
        header.style.position = 'fixed'
    }
}

window.addEventListener('scroll', function () {
    Visible(btnDown);
});

Visible(btnDown);

function toggleActive(elem) {
    elem.classList.toggle('active')
}

header__burger.addEventListener('click', () => {
    toggleActive(header__burger)
    toggleActive(header__menu)
    toggleActive(header__btn)
    toggleActive(header__logo)


    document.body.classList.toggle('lock')
})

header__list.addEventListener('click', () => {
    header__burger.classList.remove('active')
    header__menu.classList.remove('active')
    document.body.classList.toggle('lock')
})

squadArrow.addEventListener('click', () => {
    squadList.forEach((item, index) => {
        if (index >= 4) {
            item.classList.toggle('squad-col-show')
        }
    })
    squadArrow.classList.toggle('squad-arrow-open')
})

form.addEventListener("submit", formSend)

async function formSend(e) {
    e.preventDefault()

    let error = formValidate()
    const submitForm = document.querySelector('.form-submit')
    let formData = new FormData(form)

    if (error === 0) {
        submitForm.disabled = true
        try {
            let response = await fetch("https://api.ed-env.com/email", {
                method: 'POST',
                body: formData
            })
            if (response.ok) {
                alert('Ваш запрос отправлен')
                form.reset()
            }
        } catch (e) {
            alert("Ошибка Запроса")
        } finally {
            submitForm.disabled = false
        }
    }
}

function formValidate() {
    let error = 0
    let formReq = document.querySelectorAll('._req')
    formReq.forEach(item => {
        formRemoveError(item)
        if (item.classList.contains('_email')) {
            if (emailTest(item)) {
                formAddError(item)
                error++
            }
        } else if ((item.classList.contains('_sel')) && (item.value === options[0].value)) {
            formAddError(item)
            error++
        } else if (item.classList.contains('_tel')) {
            if (telegramTest(item)) {
                formAddError(item)
                error++
            }
        } else {
            if (item.value === "") {
                formAddError(item)
                error++
            }
        }

    })
    return error
}

function formAddError(item) {
    item.classList.add('_error')
}

function formRemoveError(item) {
    item.classList.remove('_error')
}

function emailTest(item) {
    return !/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(item.value)
}

function telegramTest(item) {
    return !/^@?[A-Za-z0-9_.]+.{4,}/.test(item.value)
}

function scrollToBlock(divName, offset = 75) {
    var elementPosition = document.querySelector(divName).offsetTop;
    window.scrollTo({
            top: elementPosition - offset,
            behavior: "smooth"
        }
    );
}

$('.owl-carousel').owlCarousel({
    center: true,
    items: 2,
    loop: true,
    responsive: {
        0: {
            stagePadding: 20,
            items: 1
        },
        769: {
            nav: true,
            items: 1.5
        }
    },

    lazyLoad: true,
    responseiveRefreshRate: 50,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true
});


class Tabs {
    constructor(element) {
        this.element = element

        this.tabs = null;
        this.indicators = null;

        this.prevIndex = null;
        this.nextIndex = null;
        this.activeIndex = null;

    }

    init() {
        this.tabs = this.element.querySelectorAll('.tabs-inner__item')
        this.indicators = document.querySelectorAll('.jobListPc-item')

        this.tabs.forEach((item, index) => {
            if (item.classList.contains('activeTab')) {
                this.activeIndex = index
            }
        })
        this.prevIndex = this.activeIndex <= 0 ? this.tabs.length - 1 : this.activeIndex - 1
        this.nextIndex = this.activeIndex >= this.tabs.length - 1 ? 0 : this.activeIndex + 1

    }

    slideTo(index) {
        this.init()

        this.tabs[this.activeIndex].classList.remove('activeTab')
        this.indicators[this.activeIndex].classList.remove('activeTab')

        this.tabs[index].classList.add('activeTab')
        this.indicators[index].classList.add('activeTab')
    }

    next() {
        this.init()
        this.slideTo(this.nextIndex)
    }

    prev() {
        this.init()
        this.slideTo(this.prevIndex)
    }

    switchIndicators() {
        this.init()
        const indicator = document.querySelector('.job-list')
        indicator.addEventListener('click', (event) => {
            let liIndex = null
            const li = event.target
            if (li.nodeName === "LI") {
                this.indicators.forEach((item, index) => {
                    item.classList.remove('activeTab')
                    this.tabs[index].classList.remove('activeTab')
                    if (item === li) liIndex = index
                })
                this.tabs[liIndex].classList.add('activeTab')
                this.indicators[liIndex].classList.add('activeTab')
            }
        })
    }

    prev_next() {
        const prevBtn = document.querySelector('[data-tabs="prev"]')
        const nextBtn = document.querySelector('[data-tabs="next"]')

        prevBtn.addEventListener('click', () => {
            this.prev()
        })

        nextBtn.addEventListener('click', () => {
            this.next()
        })


    }
}

const tabs = new Tabs(document.querySelector('.tabs-pc'))
tabs.init()
tabs.switchIndicators()
tabs.prev_next()


let openIdx = 0
document.querySelectorAll('.accordion-item__trigger').forEach((item,index) => {
    item.addEventListener('click', (e) => {
        const parent = item.parentNode
        if (parent.classList.contains('activeAcc')) {
            parent.classList.remove('activeAcc')
        } else {
            document.querySelectorAll('.activeAcc')
                .forEach(child => child.classList.remove('activeAcc'))

            parent.classList.add('activeAcc')
            openIdx = index
        }

    })
})

const descShow = document.querySelectorAll('.desc-toggle-show')
const skill = document.querySelectorAll('._skill-mb')

document.addEventListener('click',(e)=>{
    const target = e.target
    const parentItem = target.closest('.accordion-item')
    if (target.classList.contains('desc-toggle-show')){
        parentItem.classList.add('openSkillList')
        toggleDisplayStyle(descShow[openIdx],displayValue.none)
        toggleDisplayStyle(skill[openIdx],displayValue.block)
    }

    if (target.classList.contains('desc-toggle-hide')){
        parentItem.classList.remove('openSkillList')
        toggleDisplayStyle(descShow[openIdx],displayValue.block)
        toggleDisplayStyle(skill[openIdx],displayValue.none)
    }
})

const tabsMenu = document.querySelector('._tabs-menu')
const jobListAll = document.querySelectorAll('.job-list > li')
let tabsActiveIndex

tabsMenu.addEventListener('click',()=>{
    jobListAll.forEach((item,idx) => {
       if (item.classList.contains('activeTab')){
           tabsActiveIndex = idx
       }
    })
    if (tabsActiveIndex === 1) {
        tabsInner.classList.remove('showGrad')
    } else {
        tabsInner.classList.add('showGrad')
    }
})

tabsInner.addEventListener('scroll',()=>{
    let scrollEl = tabsInner.scrollTop

    if (scrollEl !== 0){
        tabsInner.classList.remove('showGrad')
    } else {
        tabsInner.classList.add('showGrad')
    }
})



